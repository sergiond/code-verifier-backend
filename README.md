# code-verifier-backend

## Dependencias

- Para el desarrollo de este intensivo, actualmente tenemos implementadas las dependencias con:

* Express.js: Framework para desarrollo de Back-end que va sobre Node.js.
* Jest: Framework Javascript para el testeo durante el desarrollo del proyecto
* Eslint: Herramienta para el análisis sintáctica de código
* Nodemon: Herramienta que permite el reinicio automático de la aplicación según los cambios que se realice sobre los archivos.
* WebPack: paquete con diferentes funcionalidades que nos permite asignar una serie de configuraciones para el empaquetado y optimización de los archivos.
* dotenv: librería que nos permite cargar las variables de entorno configuradas en los archivos .env

## Scripts creados:

- "build": "npx tsc" -> Script compilador de Typescript a Jascript
- "start": "node dist/index.js" -> se ejecuta el fichero compilado
- "dev": "concurrently \"npx tsc --watch\" \"nodemon -q dist/index.js \"" -> Compila e inicia el watch mode del back-end
- "test": "jest"- > Lanza test con Jest
- "serve:coverage": "npm run test && cd coverage/lcov-report && npx serve" -> pasa el test Jest y Lanza un servidor preconfigurado en localhost, en el puerto 3000 para ver los resultados cubiertos del test

## Variables de entorno (.env):

PORT=8000 : se asigna puerto 8000 por defecto para el back-end
