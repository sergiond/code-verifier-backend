import express, { Express, Request, Response } from 'express'

// swagger
import swaggerUi from 'swagger-ui-express'

// Security
import cors from 'cors' // Always on: cors allow us to create petition to different domains
import helmet from 'helmet' // extra security for express

// TODO HTTPS

// * Root Router
import rootRouter from '../routes' // import index.ts
import mongoose from 'mongoose'

// * Create Express Server -> looks in the .env  file the port, if dont fin it, we asing the 8000
const server: Express = express()

// * File Upload
const fileUpload = require('express-fileupload')
server.use(fileUpload({ createParentPath: true }))

// * Swagger Config route

server.use(
  '/docs',
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: '/swagger.json',
      explorer: true
    }
  })
)

// * Define Server to use /api and use rootRouter from 'index.ts' in routes
// From this point on over: http://localhost:8000/api/...
server.use(
  '/api',
  rootRouter
)

// Static server
server.use(express.static('public'))

// * Mongoose connection
mongoose.connect('mongodb://127.0.0.1:27017/codeverification?directConnection=true')

// * Security config
server.use(helmet())
server.use(cors())

// Content Type Config to show, limit the information sended to the server
server.use(express.urlencoded({ extended: true, limit: '50mb' }))
server.use(express.json({ limit: '50mb' }))

// * Redirection Config
// http://localhost:8000/ -> redirect to -> http://localhost:8000/api

server.get('/', (req: Request, res: Response) => {
  res.redirect('/api')
})

export default server
