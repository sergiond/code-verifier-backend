import { LogInfo } from '../utils/logger'
import express, { Request, Response } from 'express'
import { UserController } from '../controller/UsersController'

// BCRYPT for passwords
import bcrypt from 'bcrypt'

// JWT verifier MiddleWare
import { verifyToken } from '../middlewares/verifyToken.middleware'

// Body parser to read Body from requests
import bodyParser from 'body-parser'
import { IUser } from '@/domain/interfaces/IUser.interface'

// Middleware to read JSON in Body
const jsonParser = bodyParser.json()

// Router from expres

const usersRouter = express.Router()

// http://localhost:8000/api/users?id=624def9b974d2de864894e17/
usersRouter.route('/')

// GET:
  .get(verifyToken, async (req: Request, res: Response) => {
    // obtain a Query Param (ID)
    const id: any = req?.query?.id

    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10
    LogInfo(`Query Param: ${id}`)

    // Controller Instance to execute method
    const controller: UserController = new UserController()

    // Obtain Response
    const response: any = await controller.getUsers(page, limit, id)

    // Sent to the client the response and code "200"
    return res.status(200).send(response)
  })

// DELETE:
  .delete(verifyToken, async (req: Request, res: Response) => {
    // obtain a Query Param (ID)
    const id: any = req?.query?.id
    LogInfo(`Query Param: ${id}`)

    // Controller Instance to execute method
    const controller: UserController = new UserController()

    // Obtain Response
    const response: any = await controller.deleteUser(id)

    // Sent to the client the response
    return res.status(response.status).send(response)
  })

// PUT
  .put(jsonParser, verifyToken, async (req: Request, res: Response) => {
    const action = req?.body?.action
    // obtain a Query Param

    const id: any = req?.query?.id
    const name: string = req?.body?.name
    const email: string = req?.body?.email
    const age: number = req?.body?.age
    const kata: any = req?.body?.katas
    LogInfo(`Query Param: ${id}, ${name},${email},${age}`)

    // Controller Instance to execute method
    const controller: UserController = new UserController()
    const user = {
      name: name,
      email: email,
      age: age,
      katas: kata
    }
    // Obtain Response
    const response: any = await controller.updateUser(id, action, user)

    // Sent to the client the response
    return res.status(response.status).send(response)
  })

usersRouter.route('/update')
  // PUT
  .put(jsonParser, verifyToken, async (req: Request, res: Response) => {
    const id: any = req?.query?.id

    const { name, email, password, rol, age } = req?.body
    let hashedPassword = ''

    if (id && name && email && password && rol && age) {
      // Obtain the password in request and cypher
      // eslint-disable-next-line no-unused-vars
      hashedPassword = bcrypt.hashSync(req.body.password, 8)

      const updateUser: IUser = {
        id: id,
        name: name,
        email: email,
        password: hashedPassword,
        age: age,
        rol: rol,
        katas: []
      }

      // Obtain Response
      console.log(rol)
      const controller: UserController = new UserController()
      const response: any = await controller.updateUserData(updateUser)
      return res.status(200).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR: User Data missing]: No user can be registered'
      })
    }
  })

// http://localhost:8000/api/users/katas/
usersRouter.route('/katas')
  // GET:
  .get(verifyToken, async (req: Request, res: Response) => {
    // obtain a Query Param (ID)
    const id: any = req?.query?.id

    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10

    // Controller Instance to execute method
    const controller: UserController = new UserController()

    // Obtain Response
    const response: any = await controller.getKatas(page, limit, id)

    // Sent to the client the response and code "200"
    return res.status(200).send(response)
  })

export default usersRouter
