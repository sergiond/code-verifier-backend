import { BasicResponse } from '../controller/types'
import express, { Request, Response } from 'express'
import { HelloController } from '../controller/HelloController'
import { LogInfo } from '../utils/logger'

// Router from expres

const helloRouter = express.Router()

// http://localhost:8000/api/hello?name=Sergio/
helloRouter.route('/')
// GET:
  .get(async (req: Request, res: Response) => {
    // obtain a Query PAram
    const name: any = req?.query?.name
    LogInfo(`Query Param: ${name}`)
    // Controller Instance to execute method

    const controller: HelloController = new HelloController()
    // Obtain Response
    const response: BasicResponse = await controller.getMessage(name)

    // Sent to the client the response
    return res.send(response)
  })

export default helloRouter
