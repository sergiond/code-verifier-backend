import { AuthController } from '../controller/AuthController'
import { IUser } from '../domain/interfaces/IUser.interface'
import { IAuth } from '../domain/interfaces/IAuth.interface'
import express, { Request, Response } from 'express'

// BCRYPT for passwords
import bcrypt from 'bcrypt'

// JWT verifier MiddleWare
import { verifyToken } from '../middlewares/verifyToken.middleware'

// Body Parser (Read JSON from body in request POST, PUT...)

import bodyParser from 'body-parser'
import { LogError } from '../utils/logger'

// Middleware to read JSON in Body
const jsonParser = bodyParser.json()

// Router from expres

const authRouter = express.Router()

// http://localhost:8000/api/auth
authRouter.route('/')

authRouter.route('/register')
// POST
  .post(jsonParser, async (req: Request, res: Response) => {
    const { name, email, password, rol, age } = req?.body
    let hashedPassword = ''

    if (name && email && password && rol && age) {
      // Obtain the password in request and cypher
      // eslint-disable-next-line no-unused-vars
      hashedPassword = bcrypt.hashSync(req.body.password, 8)

      const newUser: IUser = {
        id: '',
        name: name,
        email: email,
        password: hashedPassword,
        age: age,
        rol: rol,
        katas: []
      }

      // Obtain Response
      console.log(rol)
      const controller: AuthController = new AuthController()
      const response: any = await controller.registerUser(newUser)
      return res.status(201).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR: User Data missing]: No user can be registered'
      })
    }
  })

authRouter.route('/login')
// POST
  .post(jsonParser, async (req: Request, res: Response) => {
    const { email, password } = req?.body
    try {
      if (email && password) {
        // Controller Instance to execute method
        const controller: AuthController = new AuthController()

        const auth: IAuth = {
          email: email,
          password: password
        }

        // Obtain response
        const response: any = await controller.loginUser(auth)
        console.log(response)
        // Send to the client the response whitch includes the Json Web token to authorize request
        return res.status(200).send(response)
      } else {
        return res.status(400).send({
          message: '[ERROR: User Data missing]: No user can be registered'
        })
      }
    } catch (error) {
      LogError(`[ERROR: User Data no match]: User is not registered ${error}`)
    }
  })

// Route Protected by Verify TOKEN middleware
authRouter.route('/me')
  .get(verifyToken, async (req: Request, res: Response) => {
    // Obtain the ID of user to check it's data
    const id: any = req?.query?.id

    if (id) {
      // Controller: Auth Controller
      const controller: AuthController = new AuthController()
      // Obtain response from Controller
      const response: any = await controller.userData(id)
      // If user is authorised:
      return res.status(200).send(response)
    } else {
      return res.status(401).send({
        message: 'You are not authorized to this action'
      })
    }
  })
export default authRouter
