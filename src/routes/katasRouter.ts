import { LogInfo } from '../utils/logger'
import express, { Request, Response } from 'express'
import { KatasController } from '../controller/KatasControllers'

// JWT verifier MiddleWare
import { decodeToken, verifyToken } from '../middlewares/verifyToken.middleware'

// Body parser to read Body from requests
import bodyParser from 'body-parser'
import { IKata, KataLevel } from '../domain/interfaces/IKatas.interface'

// Middleware to read JSON in Body
const jsonParser = bodyParser.json()

const katasRouter = express.Router()

katasRouter.route('/')
  .get(verifyToken, async (req: Request, res: Response) => {
    const id: any = req?.query?.id

    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10
    LogInfo(`Query Param: ${id}`)

    const controller: KatasController = new KatasController()
    const response: any = await controller.getKatas(page, limit, id)

    return res.send(response)
  }
  )
  .delete(jsonParser, verifyToken, async (req: Request, res: Response) => {
    // Read header 'x-access-token', decode the token and return the user id
    const token: any = decodeToken(req, res)
    // obtain a Query Param (ID)
    const id: any = req?.query?.id
    LogInfo(`Query Param: ${id}`)

    // Controller Instance to execute method
    const controller: KatasController = new KatasController()
    console.log(token.id)
    console.log(id)
    // Obtain Response
    if (id) {
      const response: any = await controller.deleteKatas(id)

      // Sent to the client the response
      return res.status(response.status).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR] Deleting Kata. You need to be the creator of the kata'
      })
    }
  })
  .put(jsonParser, verifyToken, async (req: Request, res: Response) => {
    // Read header 'x-access-token', decode the token and return the user id
    const token: any = decodeToken(req, res)
    const userID: any = req?.headers?.sessiontokenid
    const id: any = req?.query?.id
    // Read from Body
    const action: string = req?.body.action || 'Default'
    const name: string = req?.body?.name || ''
    const description: string = req?.body?.description || ''
    const level: KataLevel = req?.body?.level || KataLevel.BASIC
    const intents: number = req?.body?.intents || 0
    const stars: number = req?.body?.stars || 0
    const creator: string = req?.body?.creator
    const solution: string = req?.body?.solution || 'Default'
    const participants: string[] = req?.body?.participants
    const totalStars: number = req?.body?.totalStars || 0
    const totalUserStars: number = req?.body?.totalUserStars || 0

    if (name && description && level && intents >= 0 && stars >= 0 && creator === token.id && solution) {
      const controller: KatasController = new KatasController()

      const kata: IKata = {
        name: name,
        description: description,
        level: level,
        intents: intents,
        stars: stars,
        creator: creator,
        solution: solution,
        participants: participants,
        totalStars: totalStars,
        totalUserStars: totalUserStars
      }

      const response: any = await controller.updateKata(id, kata, userID!, action!)
      return res.status(200).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR] Updating Kata. You need to be the creator and sent all attributes of kata to update it'
      })
    }
  })
  .post(jsonParser, verifyToken, async (req: Request, res: Response) => {
  // Read header 'x-access-token', decode the token and return the user id
    const token: any = decodeToken(req, res)

    // Read from Body
    const name: string = req?.body?.name
    const description: string = req?.body?.description
    const level: KataLevel = req?.body?.level
    const intents: number = req?.body?.intents
    const stars: number = req?.body?.stars
    const creator: string = token.id
    const solution: string = req?.body?.solution
    const participants: string[] = req?.body?.participants
    const totalStars: number = 0
    const totalUserStars: number = 0

    if (name && description && level && intents >= 0 && stars >= 0 && creator && solution) {
      const controller: KatasController = new KatasController()

      const kata: IKata = {
        name: name,
        description: description,
        level: level,
        intents: intents,
        stars: stars,
        creator: creator,
        solution: solution,
        participants: participants,
        totalStars: totalStars,
        totalUserStars: totalUserStars
      }
      const response: any = await controller.createKata(kata)

      return res.status(201).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR] Creating Kata. You need to sent all attributes of kata to create it'
      })
    }
  })

// http://localhost:8000/api/katas/katasbylevel/
katasRouter.route('/katasbylevel')
  // GET:
  .get(verifyToken, async (req: Request, res: Response) => {
    // obtain a Query Param (ID)
    const level: any = req?.query?.level

    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10

    // Controller Instance to execute method
    const controller: KatasController = new KatasController()

    // Obtain Response
    const response: any = await controller.getKatasByLevel(page, limit, level)

    // Sent to the client the response and code "200"
    return res.status(200).send(response)
  })

// http://localhost:8000/api/katas/katasbystars/
katasRouter.route('/katasbystars')
  // GET:
  .get(verifyToken, async (req: Request, res: Response) => {
    // obtain a Query Param (ID)
    const stars: any = req?.query?.stars
    // Pagination
    const page: any = req?.query?.page || 1
    const limit: any = req?.query?.limit || 10

    // Controller Instance to execute method
    const controller: KatasController = new KatasController()

    // Obtain Response
    const response: any = await controller.getKatasByStars(page, limit, stars)

    // Sent to the client the response and code "200"
    return res.status(200).send(response)
  })

// http://localhost:8000/api/katas/katasolution
katasRouter.route('/katasolution')
  // POST:
  .post(jsonParser, verifyToken, async (req: Request, res: Response) => {
    // obtain a Query Param (ID)
    const id: any = req?.query?.id
    const answer: string = req?.body?.answer

    if (id && answer) {
      // Controller Instance to execute method
      const controller: KatasController = new KatasController()

      // Obtain Response
      const response: any = await controller.getKatasSolution(id, answer)
      console.log(response)
      // Sent to the client the response and code "200"
      return res.status(200).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR] cheking Kata solution. You need to be the creator and sent all attributes of kata to update it'
      })
    }
  })
// http://localhost:8000/api/katas/updatestars
katasRouter.route('/updatestars')
  // put:
  .put(jsonParser, verifyToken, async (req: Request, res: Response) => {
    // obtain a Query Param (ID)
    const id: any = req?.query?.id
    const stars: any = req?.body?.stars
    // const totalStars: number = req?.body?.totalStars || 0
    // const totalUserStars: number = req?.body?.totalUserStars || 0
    // Controller Instance to execute method
    const controller: KatasController = new KatasController()

    if (id) {
    // Obtain Response
      const response: any = await controller.updateKataStars(id, stars)

      // Sent to the client the response and code "200"
      return res.status(200).send(response)
    } else {
      return res.status(400).send({
        message: '[ERROR] Updating Kata. You need to sent all attributes of kata to create it'
      })
    }
  })
export default katasRouter
