import { DateResponse } from '../controller/types'
import express, { Request, Response } from 'express'
import { GoodbyeController } from '../controller/GoodbyeController'
import { LogInfo } from '../utils/logger'

// Router from express

const goodbyeRouter = express.Router()

// http://localhost:8000/api/goodbye?name=Sergio/

goodbyeRouter.route('/')
  .get(async (req: Request, res: Response) => {
    // Obtain a Query Param
    const name: any = req?.query?.name
    LogInfo(`Query Param: ${name}`)

    const controller: GoodbyeController = new GoodbyeController()

    // Obtain response
    const response: DateResponse = await controller.getMessage(name)

    return res.send(response)
  })

export default goodbyeRouter
