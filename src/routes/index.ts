/**
 * Root Router
 * Encargado de gestionar las redirecciones
 */

import express, { Request, Response } from 'express'
// Routes
import helloRouter from './HelloRouter'
import goodbyeRouter from './GoodbyeRouter'

import { LogInfo } from '../utils/logger'
import usersRouter from './UserRouter'
import katasRouter from './katasRouter'
import authRouter from './AuthRouter'

// Server instance

const server = express()

// Router instance

const rootRouter = express.Router()

// Activate for requests to http://localhoost:800/api

rootRouter.get('/', (req: Request, res: Response) => {
  LogInfo('GET: http://localhoost:800/api')
  res.send('Welcome to my API REST')
})

// Redirections to Routers & Controllers
server.use('/', rootRouter) // http://localhoost:800/api
server.use('/hello', helloRouter) // http://localhoost:800/api/hello -> managed by HelloRouter
server.use('/goodbye', goodbyeRouter) // http://localhost:8000/api/goodbye?name=Sergio/ -> managed by GoodbyeRouter
server.use('/users', usersRouter)// http://localhost:8000/api/users -> managed by UserRouter
server.use('/katas', katasRouter)// http://localhost:8000/api/katas -> managed by katasRouter
server.use('/auth', authRouter)// http://localhost:8000/api/auth -> managed by authRouter
// Add more routes to the app

export default server
