import { IAuth } from '../../domain/interfaces/IAuth.interface'
import { IKata } from '../../domain/interfaces/IKatas.interface'
import { IUser } from '../../domain/interfaces/IUser.interface'
import { BasicResponse, DateResponse } from '../types'
/**
 * recibe un mensaje y devuelve una promesa del tipo BasicResponse
 */
export interface IHelloController {
    getMessage(name?:string): Promise<BasicResponse>
}
/**
 * recibe un mensaje y devuelve una promesa del tipo DateResponse
 */
export interface IGoodbayController {
    getMessage(name?:string): Promise<DateResponse>
}

export interface IUserController {
    // Read users from database || get User by ID
    getUsers(page: number, limit: number, id?: string): Promise<any>
    // Get katas of User
    getKatas(page: number, limit: number, id?: string): Promise<any>
    // Delete user from database || get User by ID
    deleteUser(id?: string): Promise<any>
    // Update User
    updateUser(id: string, action: string, user:IUser): Promise<any>
}

export interface IAuthController {
    // Register User
    registerUser(user: IUser): Promise<any>
    // Loging User
    loginUser(auth: IAuth): Promise<any>
    // Logout User
    logoutUser(): Promise<any>
}
export interface IKataController {
    // Read katas from database || get Katas by ID
    getKatas(page: number, limit: number, id?: string): Promise<any>
    // Read katas from database || get Katas by Level
    getKatasByLevel(page: number, limit: number, level: string): Promise<any>
    // Read katas from database || get Katas by Stars
    getKatasByStars(page: number, limit: number, stars: number): Promise<any>
    // Create New Kata
    createKata(kata: IKata): Promise<any>
    // Delete kata from database || get Katas by ID
    deleteKatas(id?: string): Promise<any>
    // Update Kata
    updateKata(id: string, kata: IKata, userID: any, action: string): Promise<any>
    // Update stars of the Kata whitd the average
    updateKataStars(id: string, stars: number): Promise<any>
    /*
    // Upload File to Kata
    uploadKataFile(): Promise<any>
    */
}
