/**
 *  Nos devolverá un JSON con un mensaje dentro del tipo que indiquemos
 */
export type BasicResponse = {
    message: string
}
/**
 * Nos devolverá un JSON con un mensaje que indiquemos y la fecha actual
 */
export type DateResponse = {
    message: string,
    date: string
}

/**
 * Nos devolverá un JSON con un error y un mensaje
 */
export type ErrorResponse = {
    error: string,
    message: string
}

/**
 * Nos devolverá un Auth JSON con un error y un mensaje
 */
export type AuthResponse = {
    message: string,
    id: string,
    rol: string,
    token: string
}
