import { Delete, Get, Post, Put, Query, Route, Tags } from 'tsoa'
import { IKataController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'

import { getAllKatas, getKatasByID, createKata, deleteKataByID, updateKataByID, getKatasByLevel, getKatasByStars, updateKataStars, getKatasSolution } from '../domain/orm/Kata.orm'
import { IKata } from '../domain/interfaces/IKatas.interface'

@Route('api/katas')
@Tags('KatasController')

export class KatasController implements IKataController {
  @Get('/')
  public async getKatas (@Query() page: number, @Query() limit: number, @Query() id?: string): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Get Katas by ID: ${id}`)
      response = await getKatasByID(id)
    } else {
      LogSuccess('[/api/users] Get All KAtas Request')
      response = await getAllKatas(page, limit)
    }
    return response
  }

  @Post('/')
  public async createKata (kata: IKata): Promise<any> {
    let response: any = ''

    if (kata) {
      LogSuccess(`[/api/katas] Create new kata: ${kata.name}`)
      await createKata(kata).then((r) => {
        LogSuccess(`[/api/katas] Created kata: ${kata.name}`)
        response = {
          id: r._id,
          message: `Kata created successfully: ${kata}`
        }
      })
    } else {
      LogWarning(`[/api/kata] Register needs Kata Entity: ${kata}`)
      response = {
        message: 'Kata not Registered: Provide a valid kata Entity to create one'
      }
    }

    return response
  }

  @Delete('/')
  public async deleteKatas (@Query() id?: string): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/katas] Delete katas by ID: ${id}`)
      await deleteKataByID(id).then((r) => {
        response = {
          status: 200,
          message: `Kata with id ${id} deleted successfully`
        }
      })
    } else {
      LogWarning('[/api/katas] Delete kata request without ID')
      response = {
        status: 400,
        message: 'Please, provide and valid ID to remove kata from database'
      }
    }

    return response
  }

 @Put('/')
  public async updateKata (@Query() id: string, kata: IKata, userID?: any, action?: string): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/katas] Update katas by ID: ${id}`)
      await updateKataByID(id, kata, userID!, action!).then((r) => {
        response = {
          status: 200,
          message: `kata ${kata} with id ${id} updated successfully`
        }
      })
    } else {
      LogWarning('[/api/katas] Update kata request without ID')
      response = {
        status: 400,
        message: 'Please, provide and valid ID to update user from database'
      }
    }

    return response
  }

 @Get('/katasbylevel')
 public async getKatasByLevel (@Query() page: number, @Query() limit: number, @Query() level: string): Promise<any> {
   let response: any = ''
   if (level) {
     LogSuccess(`[/api/users] Get Katas by Level: ${level}`)
     response = await getKatasByLevel(page, limit, level)
   } else {
     LogSuccess('[/api/users] Get All KAtas Request')
     response = await getAllKatas(page, limit)
   }
   return response
 }

 @Get('/katasbystars')
 public async getKatasByStars (@Query() page: number, @Query() limit: number, @Query() stars: number): Promise<any> {
   let response: any = ''
   if (stars) {
     LogSuccess(`[/api/katas/katasbystars] Get Katas by Level: ${stars}`)
     response = await getKatasByStars(page, limit, stars)
   } else {
     LogWarning('[/api/katas/katasbystars] Get All Katas Request')
     response = await getAllKatas(page, limit)
   }
   return response
 }

 @Put('/updatestars')
 public async updateKataStars (@Query() id: string, stars: number): Promise<any> {
   let response: any = ''

   if (id && stars >= 0 && stars <= 5) {
     LogSuccess(`[/api/katas/updatestars] Update katas stars: ${id}`)
     await updateKataStars(id, stars).then((r) => {
       response = {
         status: 200,
         message: `kata ${id} with new stars rate ${stars} updated successfully`
       }
     })
   } else {
     LogWarning('[/api/katas/updatestars] Update kata request without ID')
     response = {
       status: 400,
       message: 'Please, provide and valid star value to update Kata from database'
     }
   }

   return response
 }

  @Post('/katasolution')
 public async getKatasSolution (@Query() id: string, answer: string): Promise<any> {
   let response: any = ''
   if (id && answer) {
     LogSuccess(`[/api/katas/katasolution] Getting Katas solution: ${answer}`)
     await getKatasSolution(id, answer).then((r) => {
       if (r) {
         console.log(`RESPONSE: ${r}`)
         response = {
           status: 200,
           message: `the kata solution with  ${id} its correct`
         }
       } else {
         LogWarning('[/api/katas/katasolution] Getting Katas solution')
         response = {
           status: 400,
           message: `The solution of the kata with ${id} its incorrect`
         }
       }
     })
   } else {
     LogWarning('[/api/katas/katasolution] Getting Katas solution')
     response = {
       status: 400,
       message: `The solution of the kata with ${id} its incorrect`
     }
   }
   return response
 }
/*
  @Post('/upload')
  uploadKataFile (): Promise<any> {
    throw new Error('Method not implemented.')
  }
  */
}
