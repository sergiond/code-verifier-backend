import { Get, Post, Query, Route, Tags } from 'tsoa'
import { IAuthController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'
import { IUser } from '../domain/interfaces/IUser.interface'
import { IAuth } from '../domain/interfaces/IAuth.interface'

// ORM imports
// eslint-disable-next-line no-unused-vars
import { registerUSer, loginUSer, logoutUser, getUserByID } from '../domain/orm/User.orm'
import { AuthResponse, ErrorResponse } from './types'

@Route('/api/auth')
@Tags('AuthController')
export class AuthController implements IAuthController {
  @Post('/register')
  public async registerUser (user: IUser): Promise<any> {
    let response: any = ''

    if (user) {
      LogSuccess(`[/api/auth/register] Register new User: ${user.email}`)
      await registerUSer(user).then((r) => {
        LogSuccess(`[/api/auth/register] Created User: ${user.email}`)
        console.log(`response: ${r}`)
        if (r !== undefined) {
          response = {
            message: `User create: ${user.name}`,
            state: true
          }
        } else {
          response = {
            message: `User NO create: ${user.name}`,
            state: false
          }
        }
      })
    } else {
      LogWarning(`[/api/auth/register] Register needs User: ${user}`)
      response = {
        message: 'User not Registered: Provide a valid User to create one'
      }
    }

    return response
  }

    @Post('/login')
  public async loginUser (auth: IAuth): Promise<any> {
    let response: AuthResponse | ErrorResponse | undefined

    if (auth) {
      LogSuccess(`[/api/auth/login] login  User: ${auth.email}`)
      const data = await loginUSer(auth)
      response = {
        token: data.token,
        id: data.user.id,
        rol: data.user.rol,
        message: `Welcome: ${data.user.name}`
      }
    } else {
      LogWarning(`[/api/auth/login] Login needs email && password: ${auth}`)
      response = {
        message: 'Please, provide a valid email && password',
        error: '[AUTH ERROR]: Email & Password are not valid'
      }
    }
    return response
  }

    /**
   * Endpoint to retrieve the Users in the Collection "Users" of Database
   * Middleware: Validate JWT
   * In header you must add the x-access-token  with a valid JWT
   * @param {string} id Id of user to retreive (optional)
   * @returns All users or users found by ID
   */
  @Get('/me')
    public async userData (@Query() id: string): Promise<any> {
      let response: any = ''
      if (id) {
        LogSuccess(`[/api/users] Get Users Data by ID: ${id}`)
        response = await getUserByID(id)
      }

      return response
    }

    @Post('/logout')
  public async logoutUser (): Promise<any> {
    const response: any = ''
    return response
  }
}
