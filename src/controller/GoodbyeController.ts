import { DateResponse } from './types'
import { IGoodbayController } from './interfaces'
import { LogSuccess } from '../utils/logger'

/**
 * Clase que gestionarará las rutas de despedida
 */
export class GoodbyeController implements IGoodbayController {
  public async getMessage (name?: string): Promise<DateResponse> {
    LogSuccess('[/api/goodbye] Get Request')
    const currentDate = new Date()
    const currentDay = currentDate.getDate()
    const currentMonth = currentDate.getMonth() + 1
    const currentYear = currentDate.getFullYear()

    const date = currentDay + '/' + currentMonth + '/' + currentYear

    return {
      message: `Goodbye, ${name || 'Anonimous'}`,
      date: date
    }
  }
}
