import { Delete, Get, Put, Query, Route, Tags } from 'tsoa'
import { IUserController } from './interfaces'
import { LogError, LogSuccess, LogWarning } from '../utils/logger'

// ORM - Users Collection
import { getAllUsers, getUserByID, deleteUserByID, updateUserByID, getKatasFromUser, updateUserDatabyID } from '../domain/orm/User.orm'
import { IUser } from '@/domain/interfaces/IUser.interface'

@Route('/api/users')
@Tags('UserController')
export class UserController implements IUserController {
  /**
   * Endpoint to retrieve the Users in the Collection "Users" of Database
   * @param {string} id Id of user to retreive (optional)
   * @returns All users or users found by ID
   */
  @Get('/')
  public async getUsers (@Query() page: number, @Query() limit: number, @Query() id?: string): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Get Users by ID: ${id}`)
      response = await getUserByID(id)
    } else {
      LogSuccess('[/api/users] Get All Users Request')
      response = await getAllUsers(page, limit)
    }

    return response
  }
  /**
   * Endpoint to delete the Users in the Collection "Users" of Database
   * @param {string} id Id of user to delete (optional)
   * @returns message informing if deletion was correct
   */

   @Delete('/')
  public async deleteUser (@Query() id?: string): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Delete Users by ID: ${id}`)
      await deleteUserByID(id).then((r) => {
        response = {
          status: 200,
          message: `user with id ${id} deleted successfully`
        }
      })
    } else {
      LogWarning('[/api/users] Delete user request without ID')
      response = {
        status: 400,
        message: 'Please, provide and valid ID to remove user from database'
      }
    }

    return response
  }

  /**
   * Endpoint to update the Users in the Collection "Users" of Database
   * @param {string} id Id to locate the user to update
   * @param {object} user data to update user
   * @returns message if the user is updated or not
   */
  @Put('/')
   public async updateUser (@Query() id: string, action: string, user: any): Promise<any> {
     let response: any = ''
     if (id) {
       LogSuccess(`[/api/users] Update User by ID: ${id}`)
       await updateUserByID(id, action, user).then((r) => {
         response = {
           status: 200,
           message: `user ${user} with id ${id} updated successfully`
         }
       })
     } else {
       LogWarning('[/api/users] Update user request without ID')
       response = {
         status: 400,
         message: 'Please, provide and valid ID to update user from database'
       }
     }

     return response
   }

  @Put('/update')
  public async updateUserData (user: IUser): Promise<any> {
    let response: any = ''

    if (user) {
      LogSuccess(`[/api/user/update'] Update User: ${user.email}`)
      response = await updateUserDatabyID(user)
    } else {
      LogWarning(`[/api/user/update] Update needs User: ${user}`)
      response = {
        message: 'User not Updated: Provide a valid User to update'
      }
    }

    return response
  }

  @Get('/katas') // users/katas
  public async getKatas (@Query() page: number, @Query() limit: number, @Query() id: string): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/users/katas] Get kata from user by ID: ${id}`)
      response = await getKatasFromUser(page, limit, id)
    } else {
      LogError('[/api/users/katas] Getting All katas')
      response = {
        message: 'ID from Users is needed'
      }
    }

    return response
  }
}
