import { IKata } from '../interfaces/IKatas.interface'

export type KatasResponse = {
    users: IKata[],
    totalPages: number,
    currentPage: number
}
