import { IKata } from '../interfaces/IKatas.interface'
import mongoose from 'mongoose'

export const kataEntity = () => {
  const kataSchema = new mongoose.Schema<IKata>({
    name: { type: String, required: true },
    description: { type: String, required: true },
    level: { type: String, required: true },
    intents: { type: Number, required: true },
    stars: { type: Number, required: true },
    creator: { type: String, required: true }, // Id of user
    solution: { type: String, required: true },
    participants: { type: [], required: true }, // List of users
    totalStars: { type: Number, required: true },
    totalUserStars: { type: Number, required: true }
  })

  return mongoose.models.Katas || mongoose.model<IKata>('Katas', kataSchema)
}
