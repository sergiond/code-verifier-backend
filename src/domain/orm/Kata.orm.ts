import { kataEntity } from '../entities/kata.entity'
import { LogError, LogSuccess } from '../../utils/logger'
import { IKata } from '../interfaces/IKatas.interface'

// Environment variables
import dotenv from 'dotenv'

// Configuration of environment variables
dotenv.config()

// CRUD

/**
 * Method to obtain all Katas from Collection "Katas" in Mongo Server
 */

export const getAllKatas = async (page: number, limit: number): Promise<any[] | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}

    LogSuccess('[ORM SUCCESS]: Getting All Katas')
    await kataModel.find({ isDelete: false })
      .select('name description level intents stars creator solution')
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: IKata[]) => {
        response.katas = katas
      })

    await kataModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Katas:${error}`)
  }
}

export const getKatasByID = async (_id: string): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    return await kataModel.findById({ _id })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Katas:${error}`)
  }
}

export const getKatasSolution = async (_id: string, answer: string): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    let response: any = ''

    await kataModel.findById({ _id })
      .select('name desciption solution')
      .exec().then((katas: IKata) => {
        LogSuccess('[ORM SUCCESS]: Getting kata solution')
        if (katas.solution === answer) {
          response = true
        } else {
          response = false
        }
      })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting kata solution:${error}`)
  }
}

export const getKatasByLevel = async (page: number, limit: number, level: string): Promise<any[] | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}

    LogSuccess('[ORM SUCCESS]: Getting All Katas filtered by level')
    await kataModel.find({ level: level })
      .select('name description level intents stars creator solution')
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: IKata[]) => {
        response.katas = katas
      })

    await kataModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Katas filtered by level:${error}`)
  }
}

export const getKatasByStars = async (page: number, limit: number, stars: number): Promise<any[] | undefined> => {
  try {
    const kataModel = kataEntity()
    const response: any = {}

    LogSuccess('[ORM SUCCESS]: Getting All Katas filtered by stars')
    await kataModel.find({ stars: stars })
      .select('name description stars intents stars creator solution')
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((katas: IKata[]) => {
        response.katas = katas
      })

    await kataModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })
    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Katas filtered by stars:${error}`)
  }
}

export const deleteKataByID = async (id: string):Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    // Search User By ID
    return await kataModel.deleteOne({ _id: id })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting User by ID:${error}`)
  }
}

export const createKata = async (kata: IKata):Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    // Create / insert new Kata
    return await kataModel.create(kata)
  } catch (error) {
    LogError(`[ORM ERROR]: Creating Kata:${error}`)
  }
}

export const updateKataByID = async (id: string, kata: IKata, userID: any, action: string): Promise<any | undefined> => {
  try {
    const kataModel = kataEntity()
    if (action === 'add') {
      return await kataModel.findByIdAndUpdate({ _id: id }, { $addToSet: { participants: userID } }, { new: true })
    } else {
      return await kataModel.findByIdAndUpdate({ _id: id }, kata, { new: true })
    }
  } catch (error) {
    LogError(`[ORM ERROR]: Updating User ${id}:${error}`)
  }
}

export const updateKataStars = async (id: string, stars: number): Promise<any | undefined> => {
  let meanstars: number = 0
  let totalStars: number = 0
  let totalUserStars: number = 0

  try {
    const kataModel = kataEntity()
    await kataModel.findById({ _id: id })
      .exec()
      .then(async (kata: IKata) => {
        totalUserStars = kata.totalUserStars + 1
        totalStars = kata.totalStars + Number(stars)
        meanstars = Math.round(totalStars / totalUserStars)
        return await kataModel.findByIdAndUpdate({ _id: id }, { stars: meanstars, totalStars: totalStars, totalUserStars: totalUserStars })
      })
  } catch (error) {
    LogError(`[ORM ERROR]: Updating Kata ${id}:${error}`)
  }
}
