import { userEntity } from '../entities/user.entity'
import { LogError } from '../../utils/logger'
import { IUser } from '../interfaces/IUser.interface'
import { IAuth } from '../interfaces/IAuth.interface'
import { kataEntity } from '../entities/kata.entity'
// Enviroment variables
import dotenv from 'dotenv'

// BCRYPT from password
import bcrypt from 'bcrypt'

// JWT
import jwt from 'jsonwebtoken'
import { IKata } from '../interfaces/IKatas.interface'
// import { UserResponse } from '../types/UsersResponse.type'

// Configuration of enviroment variables
dotenv.config()

// Obtain Secret key to generate JWT
const secret = process.env.SECRETKEY

// CRUD

/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 */
export const getAllUsers = async (page: number, limit: number):Promise<any[] | undefined> => {
  try {
    const userModel = userEntity()

    const response: any = {}
    // Search all users (using pagination)

    // Search all users
    await userModel.find({ isDelete: false })
      .select('name email age katas')
      .limit(limit)
      .skip((page - 1) * limit)
      .exec().then((users: IUser[]) => {
        response.users = users
      })

    // Count total documents in collection "Users"

    await userModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })

    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Users:${error}`)
  }
}

// Get user by ID
export const getUserByID = async (_id: string):Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Search User By ID
    return await userModel.findById({ _id }).select('name email age katas')
  } catch (error) {
    LogError(`[ORM ERROR]: Getting User by ID:${error}`)
  }
}

// Delete User

export const deleteUserByID = async (_id: string):Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Search User By ID
    return await userModel.deleteOne({ _id })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting User by ID:${error}`)
  }
}

// Add new kata ID to user filtered by id
export const updateUserByID = async (id: string, action:string, user: IUser): Promise<any | undefined> => {
  try {
    // if the action is ADD, then include the ID to the katas array of the user, in other case remove the ID from the array
    const userModel = userEntity()
    if (action === 'add') {
      return await userModel.findByIdAndUpdate({ _id: id }, { $addToSet: { katas: user.katas } }, { new: true })
    } else {
      return await userModel.findByIdAndUpdate({ _id: id }, { $pull: { katas: user.katas } }, { new: true })
    }
  } catch (error) {
    LogError(`[ORM ERROR]: Updating User ${id}:${error}`)
  }
}

// Add new kata ID to user filtered by id
export const updateUserDatabyID = async (user: IUser): Promise<any | undefined> => {
  try {
    // if the action is ADD, then include the ID to the katas array of the user, in other case remove the ID from the array
    const userModel = userEntity()
    console.log(user.id)
    return await userModel.findByIdAndUpdate({ _id: user.id }, user, { new: true })
  } catch (error) {
    LogError(`[ORM ERROR]: Updating User ${user.id}:${error}`)
  }
}

// Register User
export const registerUSer = async (user: IUser): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Create / insert new User
    return await userModel.create(user)
  } catch (error) {
    LogError(`[ORM ERROR]: Creating User:${error}`)
  }
}

// Login User
export const loginUSer = async (auth: IAuth): Promise<any | undefined> => {
  try {
    const userModel = userEntity()

    let userFound: IUser | undefined
    let token: string | null = null

    // Check if user exists by unique Email
    await userModel.findOne({ email: auth.email }).then((user: IUser) => {
      userFound = user
    }).catch((error) => {
      console.log('[ERROR Auth in ORM ]: User not found')
      throw new Error(`[ERROR Auth in ORM ]: User not found ${error}`)
    })

    // check is password is Valid (compara with bcrypt)
    const validPassword = bcrypt.compareSync(auth.password, userFound!.password)

    // generate JWT
    token = jwt.sign({ id: userFound!.id }, secret!, {
      expiresIn: '3h'
    })

    if (!validPassword || token === null) {
      console.log('[ERROR Auth in ORM ]: Password Not Valid')
      throw new Error('[ERROR Auth in ORM ]: Password Not Valid')
    }

    return {
      user: userFound,
      token: token
    }
  } catch (error) {
    LogError(`[ORM ERROR] login User: ${error}`)
  }
}

// Logout User
export const logoutUser = async (): Promise<any | undefined> => {
// Managed at front
}

export const getKatasFromUser = async (page: number, limit: number, id: string):Promise<any[] | undefined> => {
  try {
    const userModel = userEntity()
    const kataModel = kataEntity()

    const response: any = {}
    // Search all users (using pagination)
    // Obtain User's Katas
    await userModel.findById(id).then(async (user: IUser) => {
      response.user = user.email

      await kataModel.find({ _id: { $in: user.katas } }).then((katas: IKata[]) => {
        response.katas = katas
      })
    }).catch((error) => {
      LogError(`[ORM ERROR]: Obtaining users: ${error}`)
    })

    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Users:${error}`)
  }
}
